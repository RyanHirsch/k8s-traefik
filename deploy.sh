#!/usr/bin/env bash

local_dir="$( cd "$( dirname "$0" )" && pwd )"

echo "Starting deployment of local traefik"
kubectl apply -f "${local_dir}/k8s"
