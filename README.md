# Creating and routing web traffic to local k8s

Adapted from <https://icicimov.github.io/blog/kubernetes/Kubernetes-cluster-step-by-step-Part8/>

Points of change include:

- Removing SSL redirect because I only plan on running locally

Steps:

1. `kubectl create -f traefik-rbac.yml`
1. `kubectl create -f traefik-config-map.yml`
1. `kubectl create -f traefik-service-account.yml`
1. `kubectl create -f traefik-service.yml`
1. `kubectl create -f traefik-deployment.yml`

Notes:

- Existing ConfigMap can be replaced with `kubectl replace -f traefik-config-map.yml`
- Dashboard available at <http://traefik.dev.lan:30088/dashboard/>
- Applications available at <http://[app].dev.lan:30080>
  - The app name will be defined in the `ingress.yaml`, see example project <https://gitlab.com/RyanHirsch/k8s-express>
